
/*
	Copyright (c) 2009 by Chad Nelson
	Released under the MIT License.
	See the provided LICENSE.TXT file for details.
*/

#ifndef MARKDOWN_H_INCLUDED
#define MARKDOWN_H_INCLUDED

#include <iostream>
#include <sstream>
#include <string>
#include <list>

//#include <boost/noncopyable.hpp>
//#include <boost/shared_ptr.hpp>
//#include <boost/optional.hpp>
//#include <boost/unordered_map.hpp>

#include <memory> // std::shared_ptr
#include <optional.hpp> // std::experimental::optional
#include <unordered_map> // std::unordered_map
// replace of boost::noncopyable for C++0x 
class NonCopyable {
protected:
    NonCopyable () {}
    ~NonCopyable () {} /// Protected non-virtual destructor
private:
   // copy and assignment not allowed
   NonCopyable( const NonCopyable& ) = delete;
   void operator=( const NonCopyable& ) = delete;
};

namespace markdown {

	using std::experimental::optional;
	using std::experimental::nullopt;

	// Forward references.
	class Token;
	class LinkIds;

	typedef std::shared_ptr<Token> TokenPtr;
	typedef std::list<TokenPtr> TokenGroup;

	class Document: private NonCopyable {
		public:
		Document(size_t spacesPerTab=cDefaultSpacesPerTab);
		Document(std::istream& in, size_t spacesPerTab=cDefaultSpacesPerTab);
		~Document();

		// You can call read() functions multiple times before writing if
		// desirable. Once the document has been processed for writing, it can't
		// accept any more input.
		bool read(const std::string&);
		bool read(std::istream&);
		void write(std::ostream&);
		void writeTokens(std::ostream&); // For debugging

		// The class is marked noncopyable because it uses reference-counted
		// links to things that get changed during processing. If you want to
		// copy it, use the `copy` function to explicitly say that.
		Document copy() const; // TODO: Copy function not yet written.

		private:
		bool _getline(std::istream& in, std::string& line);
		void _process();
		void _mergeMultilineHtmlTags();
		void _processInlineHtmlAndReferences();
		void _processBlocksItems(TokenPtr inTokenContainer);
		void _processParagraphLines(TokenPtr inTokenContainer);

		static const size_t cSpacesPerInitialTab, cDefaultSpacesPerTab;

		const size_t cSpacesPerTab;
		TokenPtr mTokenContainer;
		LinkIds *mIdTable;
		bool mProcessed;
	};

} // namespace markdown

#endif // MARKDOWN_H_INCLUDED
